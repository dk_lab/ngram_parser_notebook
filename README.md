# ngram_parser_notebook
##NLTK based ngram and POS-ngram parser

###Requirements:
*  NLTK
*  NLTK downloads:

```
#!python
#in terminal type
import nltk
nltk.download() 

```

###A pop up window appears. In the window choose:
* Models => Treebanks Part of Speech Tagger, Treebank Part of Speech Tagger (Maximum entropy), Punkt Tokenizer Models, WordNet